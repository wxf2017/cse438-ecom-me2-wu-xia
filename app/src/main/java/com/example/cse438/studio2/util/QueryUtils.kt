package com.example.cse438.studio2.util

import android.text.TextUtils
import android.util.Log
import com.example.cse438.studio2.model.Offer
import com.example.cse438.studio2.model.Product
import com.example.cse438.studio2.model.SiteDetail
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL
import java.nio.charset.Charset
import kotlin.collections.ArrayList

class QueryUtils {
    companion object {
        private val LogTag = this::class.java.simpleName
        private const val BaseURL = "http://ec2-54-147-241-200.compute-1.amazonaws.com:3000/api/apple_products" // localhost URL

        fun fetchProductData(jsonQueryString: String): ArrayList<Product>? {
            //TODO: Implement this method from the provided pseudocode
            var url: URL = URL(BaseURL)
            var jsonResponse = ""

            try {
                makeHttpRequest(url)
            }catch (e:Exception) {
                Log.e(this.LogTag, e.toString())
            }
            var list: ArrayList<Product> = ArrayList()

            return list
        }

        private fun createUrl(stringUrl: String): URL? {
            //TODO: Implement this method from the provided pseudocode
            var url:URL = URL(null)
            try {
                url = URL(BaseURL)
            }catch (e:Exception){
                Log.e(this.LogTag, e.toString())
            }

            return url
        }

        private fun makeHttpRequest(url: URL?): String {
            var jsonResponse = ""

            if (url == null) {
                return jsonResponse
            }

            var urlConnection: HttpURLConnection? = null
            var inputStream: InputStream? = null
            try {
                urlConnection = url.openConnection() as HttpURLConnection
                urlConnection.readTimeout = 10000 // 10 seconds
                urlConnection.connectTimeout = 15000 // 15 seconds
                urlConnection.requestMethod = "GET"
                urlConnection.connect()

                if (urlConnection.responseCode == 200) {
                    inputStream = urlConnection.inputStream
                    jsonResponse = readFromStream(inputStream)
                }
                else {
                    Log.e(this.LogTag, "Error response code: ${urlConnection.responseCode}")
                }
            }
            catch (e: IOException) {
                Log.e(this.LogTag, "Problem retrieving the product data results: $url", e)
            }
            finally {
                urlConnection?.disconnect()
                inputStream?.close()
            }

            return jsonResponse
        }

        private fun readFromStream(inputStream: InputStream?): String {
            val output = StringBuilder()
            if (inputStream != null) {
                val inputStreamReader = InputStreamReader(inputStream, Charset.forName("UTF-8"))
                val reader = BufferedReader(inputStreamReader)
                var line = reader.readLine()
                while (line != null) {
                    output.append(line)
                    line = reader.readLine()
                }
            }

            return output.toString()
        }

        private fun extractDataFromJson(productJson: String?): ArrayList<Product>? {
            if (TextUtils.isEmpty(productJson)) {
                return null
            }

            val productList = ArrayList<Product>()
            try {
                val baseJsonResponse = JSONArray(productJson)
                for (i in 0 until baseJsonResponse.length()) {
                    val productObject = baseJsonResponse.getJSONObject(i)

                    // Images
                    val images = returnValueOrDefault<JSONArray>(productObject, "images") as JSONArray?
                    val imageArrayList = ArrayList<String>()
                    if (images != null) {
                        for (j in 0 until images.length()) {
                            imageArrayList.add(images.getString(j))
                        }
                    }

                    // Features
                    val features = returnValueOrDefault<JSONObject>(productObject, "features") as JSONObject?
                    val featureDictionary = mutableMapOf<String, Any>()
                    if (features != null) {
                        val featureKeys = features.keys()
                        while (featureKeys.hasNext()) {
                            val key = featureKeys.next()
                            featureDictionary[key] = features[key]
                        }
                    }

                    // Site Details
                    val siteDetails = returnValueOrDefault<JSONArray>(productObject, "sitedetails") as JSONArray?
                    val detailArrayList = ArrayList<SiteDetail>()
                    if (siteDetails != null) {
                        for (j in 0 until siteDetails.length()) {
                            val detail = siteDetails.getJSONObject(j)
                            val offers = returnValueOrDefault<JSONArray>(detail, "latestoffers") as JSONArray?
                            val offerArrayList = ArrayList<Offer>()
                            if (offers != null) {
                                for (k in 0 until offers.length()) {
                                    val offer = offers.getJSONObject(k)
                                    offerArrayList.add(Offer(
                                        returnValueOrDefault<String>(offer, "id") as String,
                                        returnValueOrDefault<String>(offer, "currency") as String,
                                        returnValueOrDefault<Long>(offer, "firstrecorded_at") as Long,
                                        returnValueOrDefault<Double>(offer, "price") as Double,
                                        returnValueOrDefault<Long>(offer, "lastrecorded_at") as Long,
                                        returnValueOrDefault<String>(offer, "seller") as String,
                                        returnValueOrDefault<String>(offer, "condition") as String,
                                        returnValueOrDefault<String>(offer, "availability") as String,
                                        returnValueOrDefault<Int>(offer, "isactive") as Int
                                    ))
                                }
                            }

                            detailArrayList.add(SiteDetail(
                                returnValueOrDefault<String>(detail, "url") as String,
                                returnValueOrDefault<String>(detail, "name") as String,
                                returnValueOrDefault<String>(detail, "sku") as String,
                                returnValueOrDefault<Int>(detail, "recentoffers_count") as Int,
                                offerArrayList
                            ))
                        }
                    }

                    //TODO: Finish implementing this method

                }
            }
            catch (e: JSONException) {
                Log.e(this.LogTag, "Problem parsing the product JSON results", e)
            }

            return productList
        }

        private inline fun <reified T> returnValueOrDefault(json: JSONObject, key: String): Any? {
            when (T::class) {
                String::class -> {
                    return if (json.has(key)) {
                        json.getString(key)
                    } else {
                        ""
                    }
                }
                Int::class -> {
                    return if (json.has(key)) {
                        json.getInt(key)
                    }
                    else {
                        return -1
                    }
                }
                Double::class -> {
                    return if (json.has(key)) {
                        json.getDouble(key)
                    }
                    else {
                        return -1.0
                    }
                }
                Long::class -> {
                    return if (json.has(key)) {
                        json.getLong(key)
                    }
                    else {
                        return (-1).toLong()
                    }
                }
                JSONObject::class -> {
                    return if (json.has(key)) {
                        json.getJSONObject(key)
                    }
                    else {
                        return null
                    }
                }
                JSONArray::class -> {
                    return if (json.has(key)) {
                        json.getJSONArray(key)
                    }
                    else {
                        return null
                    }
                }
                else -> {
                    return null
                }
            }
        }
    }
}